const express = require("express");
const app = express();
require("dotenv").config();

const mongoose = require("mongoose");
const helmet = require("helmet");
const morgan = require("morgan");

// Routes
const userRoute = require("./routes/users");
const authRoute = require("./routes/auth");

mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true }, () => {
  console.log("Connected to database!");
});

// Middleware
app.use(express.json());
app.use(helmet());
app.use(morgan("common")); // Logging

app.use("/api/users", userRoute);
app.use("/api/auth", authRoute);

app.listen(8000, () => {
  console.log("Backend server is running!");
});
